import java.math.BigDecimal;                                                                                                                                                        
import java.math.RoundingMode;                                                                                                                                                      
import java.util.Scanner;                                                                                                                                                           
                                                                                                                                                                                    
/**                                                                                                                                                                                 
 *                                                                                                                                                                                  
 * @author jimmy                                                                                                                                                                    
 */                                                                                                                                                                                 
public class Average {                                                                                                                                                              
                                                                                                                                                                                    
    public static void main(String[] args) {                                                                                                                                        
        BigDecimal sum = BigDecimal.ZERO;                                                                                                                                           
        long n = 0;                                                                                                                                                                 
                                                                                                                                                                                    
        Scanner in = new Scanner(System.in);                                                                                                                                        
        while (in.hasNext()) {                                                                                                                                                      
            sum = sum.add(in.nextBigDecimal());
            n++;
        }
        System.out.println(sum.divide(BigDecimal.valueOf(n), 10, RoundingMode.HALF_DOWN));
    }
}
